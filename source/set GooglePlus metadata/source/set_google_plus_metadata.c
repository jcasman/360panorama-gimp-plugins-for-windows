/**
 * Copyright 2013 Michał Rudewicz
 * 
 * Gimp plugin that adds metadata necessary for Google+ to imterprete picture as equirectangular panorama
 * 
 */
#include "config.h"

#if HAVE_LIBEXEMPI

#include <libgimp/gimp.h>
#include <math.h>
#include <exempi/xmp.h>
#include <string.h>

#define METADATA_PARASITE   "gimp-metadata"
#define METADATA_MARKER     "GIMP_XMP_1"
#define METADATA_MARKER_LEN (sizeof (METADATA_MARKER) - 1)
#define PANORAMA_NAMESPACE "http://ns.google.com/photos/1.0/panorama/"

static void query(void);
static void run(const gchar *name,
        gint nparams,
        const GimpParam *param,
        gint *nreturn_vals,
        GimpParam **return_vals);

GimpPlugInInfo PLUG_IN_INFO = {
    NULL,
    NULL,
    query,
    run
};

MAIN()

static void
query(void) {
    static GimpParamDef args[] = {
        {
            GIMP_PDB_INT32,
            "run-mode",
            "Run mode"
        },
        {
            GIMP_PDB_IMAGE,
            "image",
            "Input image"
        },
        {
            GIMP_PDB_DRAWABLE,
            "drawable",
            "Input drawable"
        }
    };

    gimp_install_procedure(
            "plug-in-set-google-plus-metadata",
            "Set Google+ metadata for panorama",
            "Set metadata that makes image to be recognized as 360 Panorama by Google+",
            "Michał Rudewicz",
            "Copyright Michał Rudewicz",
            "2013",
            "_Insert Google+ metadata",
            "RGB*, GRAY*",
            GIMP_PLUGIN,
            G_N_ELEMENTS(args), 0,
            args, NULL);

    gimp_plugin_menu_register("plug-in-set-google-plus-metadata",
            "<Image>/Filters/360 Panorama");
}

static void
run(const gchar *name,
        gint nparams,
        const GimpParam *param,
        gint *nreturn_vals,
        GimpParam **return_vals) {
    static GimpParam values[1];
    GimpPDBStatusType status = GIMP_PDB_EXECUTION_ERROR;
    gint32 imageId;


#ifdef _DEBUG
    /* Uncomment to send XMP logging to a file (e.g. if XMPCore lib was compiled with the Trace_ParsingHackery directive)
    FILE *stream;
    freopen_s(&stream, "c:\\tmp\\gimpplugin_stderr.txt", "w", stderr);
    freopen_s(&stream, "c:\\tmp\\gimpplugin_stdout.txt", "w", stdout);
    */

    // Poor man's JIT debugger!
    //while (1) {}
#endif

    /* Setting mandatory output values */
    *nreturn_vals = 1;
    *return_vals = values;

    values[0].type = GIMP_PDB_STATUS;
    values[0].data.d_status = status;

    imageId = param[1].data.d_image;

    if (!xmp_init())return;
    XmpPtr myXmp;

    GimpParasite *metadata = gimp_image_parasite_find(imageId, METADATA_PARASITE);
    if (metadata) {
        if (strncmp(METADATA_MARKER, gimp_parasite_data(metadata), METADATA_MARKER_LEN) == 0) {
            myXmp = xmp_new((char*) gimp_parasite_data(metadata) + METADATA_MARKER_LEN, gimp_parasite_data_size(metadata) - METADATA_MARKER_LEN);
            gimp_parasite_free(metadata);

            if (!myXmp) {
                // xmp_new() failed
                //
                // I've seen this happen when ParseRDF encounters a duplicate property,
                // causing error "Duplicate property or field node".
                // It seems to be quite common for exif properties to end up duplicated
                // and exif:ImageWidth is a frequent example.
                //
                // This is invalid according to the spec, and I haven't found a way to have
                // the parser overlook this kind of invalid formatting.
                // See https://forums.adobe.com/thread/2355028
                // and section 6.1 of http://wwwimages.adobe.com/www.adobe.com/content/dam/acom/en/devnet/xmp/pdfs/XMP%20SDK%20Release%20cc-2016-08/XMPSpecificationPart1.pdf
                //     "NOTE 1   The restriction for unique names means that it is invalid to
				//     have multiple occurrences of the same property name in an XMP packet."
                //
                // I suggest in GIMP going File->Properties->Advanced->Export XMP... then examine
                // the file for duplicated properties, remove them, and import the XMP.
                gimp_message("The image's previous metadata could not be preserved (couldn't parse, likely the image contained an invalid/dupl. XMP property)");
                myXmp = xmp_new_empty();
            }
        } else {
            myXmp = xmp_new_empty();
        }
        gimp_image_parasite_detach(imageId, METADATA_PARASITE);
    } else {
        myXmp = xmp_new_empty();
    }
    XmpStringPtr prefix = xmp_string_new();
    XmpStringPtr buf = xmp_string_new();

    if (!xmp_register_namespace(PANORAMA_NAMESPACE, "GPano", prefix)) return;
    xmp_delete_property(myXmp, PANORAMA_NAMESPACE, "UsePanoramaViewer");
    xmp_delete_property(myXmp, PANORAMA_NAMESPACE, "ProjectionType");
    xmp_delete_property(myXmp, PANORAMA_NAMESPACE, "CroppedAreaImageWidthPixels");
    xmp_delete_property(myXmp, PANORAMA_NAMESPACE, "CroppedAreaImageHeightPixels");
    xmp_delete_property(myXmp, PANORAMA_NAMESPACE, "FullPanoWidthPixels");
    xmp_delete_property(myXmp, PANORAMA_NAMESPACE, "FullPanoHeightPixels");
    xmp_delete_property(myXmp, PANORAMA_NAMESPACE, "CroppedAreaLeftPixels");
    xmp_delete_property(myXmp, PANORAMA_NAMESPACE, "CroppedAreaTopPixels");

    xmp_set_property(myXmp, PANORAMA_NAMESPACE, "UsePanoramaViewer", "True", 0);
    xmp_set_property(myXmp, PANORAMA_NAMESPACE, "ProjectionType", "equirectangular", 0);
    xmp_set_property_int32(myXmp, PANORAMA_NAMESPACE, "CroppedAreaImageWidthPixels", gimp_image_width(imageId), 0);
    xmp_set_property_int32(myXmp, PANORAMA_NAMESPACE, "CroppedAreaImageHeightPixels", gimp_image_height(imageId), 0);
    xmp_set_property_int32(myXmp, PANORAMA_NAMESPACE, "FullPanoWidthPixels", gimp_image_width(imageId), 0);
    xmp_set_property_int32(myXmp, PANORAMA_NAMESPACE, "FullPanoHeightPixels", gimp_image_height(imageId), 0);
    xmp_set_property_int32(myXmp, PANORAMA_NAMESPACE, "CroppedAreaLeftPixels", 0, 0);
    xmp_set_property_int32(myXmp, PANORAMA_NAMESPACE, "CroppedAreaTopPixels", 0, 0);

    // Build parasite metadata
    xmp_serialize(myXmp, buf, XMP_SERIAL_ENCODEUTF8, 0);
    gchar *newdata = g_strconcat(METADATA_MARKER, xmp_string_cstr(buf),NULL);
    metadata = gimp_parasite_new(METADATA_PARASITE, GIMP_PARASITE_PERSISTENT, strlen(newdata), newdata);
    g_free(newdata);
    gimp_image_parasite_attach(imageId, metadata);
    gimp_parasite_free(metadata);


    xmp_string_free(buf);
    xmp_string_free(prefix);
    xmp_free(myXmp);

    values[0].data.d_status = GIMP_PDB_SUCCESS;

    return;
}

#else
#error No libexempi
#endif

