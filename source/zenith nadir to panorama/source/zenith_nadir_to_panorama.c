/**
 * Copyright 2013 Michał Rudewicz
 * 
 * Gimp plugin that fills the top and the bottom empty areas
 * 
 */

#include "config.h"
#include <libgimp/gimp.h>
#define _USE_MATH_DEFINES
#include <math.h>

static void query(void);
static void run(const gchar *name,
		gint nparams,
		const GimpParam *param,
		gint *nreturn_vals,
		GimpParam **return_vals);
static void ZenithAndNadirToPanorama(GimpDrawable *drawable);

GimpPlugInInfo PLUG_IN_INFO = {
	NULL,
	NULL,
	query,
	run
};

MAIN()

static void
query(void) {
	static GimpParamDef args[] = {
		{
			GIMP_PDB_INT32,
			"run-mode",
			"Run mode"
		},
		{
			GIMP_PDB_IMAGE,
			"image",
			"Input image"
		},
		{
			GIMP_PDB_DRAWABLE,
			"drawable",
			"Input drawable"
		}
	};

	gimp_install_procedure(
			"plug-in-zenith-and-nadir-to-panorama",
			"Zenith and nadir to panorama",
			"Convert editable zenith and nadir to 360 panorama",
			"Michał Rudewicz",
			"Copyright Michał Rudewicz",
			"2013",
			"_Zenith & nadir to panorama",
			"RGB*, GRAY*",
			GIMP_PLUGIN,
			G_N_ELEMENTS(args), 0,
			args, NULL);

	gimp_plugin_menu_register("plug-in-zenith-and-nadir-to-panorama",
			"<Image>/Filters/360 Panorama");
}

static void
run(const gchar *name,
		gint nparams,
		const GimpParam *param,
		gint *nreturn_vals,
		GimpParam **return_vals) {
	static GimpParam values[1];
	GimpPDBStatusType status = GIMP_PDB_SUCCESS;
	GimpDrawable *drawable;
	gint32 imageId;

	/* Setting mandatory output values */
	*nreturn_vals = 1;
	*return_vals = values;

	values[0].type = GIMP_PDB_STATUS;
	values[0].data.d_status = status;

	drawable = gimp_drawable_get(param[2].data.d_drawable);
	imageId = param[1].data.d_image;
	gimp_selection_none(imageId);
#ifdef DO_TIMING
	GTimer* timer = g_timer_new();
#endif

	ZenithAndNadirToPanorama(drawable);

#ifdef DO_TIMING
	gulong nothing;
	g_print("ZenithAndNadirToPanorama() took %g seconds.\n", g_timer_elapsed(timer, &nothing));
	g_timer_destroy(timer);
#endif
	gimp_displays_flush();
	gimp_drawable_detach(drawable);

	return;
}

static void
ZenithAndNadirToPanorama(GimpDrawable *drawable) {
	static gint channels;
	static gint height, width;
	static GimpPixelRgn rgn_in, rgn_out;
	static guchar *rectIn, *rectOut;
	static gint step;
	static gdouble X, Y;
	static gdouble xCenter, yCenter;
	static gint inclusiveWidth;

	//	gimp_drawable_mask_bounds(drawable->drawable_id, &x1, &, &x2, &y2);
	channels = gimp_drawable_bpp(drawable->drawable_id);

	width = gimp_drawable_width(drawable->drawable_id);
	height = gimp_drawable_height(drawable->drawable_id);

	gimp_pixel_rgn_init(&rgn_in,
			drawable, 0, 0, width, height, FALSE, FALSE);
	gimp_pixel_rgn_init(&rgn_out,
			drawable, 0, 0, width, height, TRUE, TRUE);

	// Correct step to number of threads
	step = MAX(10, height / 50);

	/* Allocate memory for Input and output rectangles */
	rectIn = g_new(guchar, channels * width * height);
	rectOut = g_new(guchar, channels * width * height);

	xCenter = (width - 1) / 2.0, yCenter = (height - 1) / 2.0;
	inclusiveWidth = width - 1;
	gint output;

	X = ((gdouble) (width - xCenter))*2 / (gdouble) inclusiveWidth;
	Y = ((gdouble) (height - yCenter))*2 / (gdouble) inclusiveWidth;

	gimp_progress_init("Converting zenith and nadir to panorama");
	gimp_pixel_rgn_get_rect(&rgn_in, rectIn, 0, 0, width, height);

	gint x, y, channel;
	gdouble xNorm, yNorm, xx, yy, xc, xxx, yyy, cosc;
	gint sinphi1, inX, inY;

	for (y = 0; y < height; y++) {
		yNorm = ((gdouble) (y - yCenter))*2 / (gdouble) inclusiveWidth;
		for (x = 0; x < width; x++) {
			xNorm = ((gdouble) (x - xCenter))*2 / (gdouble) inclusiveWidth;

			output = 1;
			if (yNorm > Y / 4) {
				sinphi1 = 1;
				xc = -X / 2;
			} else if (yNorm < (-Y / 4)) {
				sinphi1 = -1;
				xc = X / 2;
			} else {
				output = 0;
			}

			if (output) {
				cosc = sinphi1 * sin(yNorm / Y * M_PI_2);
				xx = cos(yNorm / Y * M_PI_2) * sin(xNorm / X * M_PI) / cosc;
				yy = -sinphi1 * cos(yNorm / Y * M_PI_2) * cos(xNorm / X * M_PI) / cosc;
				if (fabs(xx) >= 0.99) output = 0;
			}
			if (output) {
				xxx = xx * X / 2 + xc;
				yyy = yy*Y;
				//            inX = (width * xxx - 1) / 2 + xCenter;
				//            inY = (width * yyy - 1) / 2 + yCenter;
				inX = round((inclusiveWidth * xxx - 1) / 2 + xCenter);
				inY = round((inclusiveWidth * yyy - 1) / 2 + yCenter);
			}

			if (output && inX >= 0 && inX < width && inY >= 0 && inY < height) {
				for (channel = 0; channel < channels; channel++) {
					rectOut[channels * (x + width * y) + channel] = rectIn[channels * (inX + width * inY) + channel];
				}
			} else {
				for (channel = 0; channel < channels; channel++) {
					rectOut[channels * (x + width * y) + channel] = 0;
				}
			}

		}
		if (y % step == 0)
			gimp_progress_update((gdouble) y / (gdouble) height);
	}
	gimp_progress_update(1);
	gimp_pixel_rgn_set_rect(&rgn_out, rectOut, 0, 0, width, height);

	g_free(rectIn);
	g_free(rectOut);

	gimp_drawable_flush(drawable);
	gimp_drawable_merge_shadow(drawable->drawable_id, TRUE);
	gimp_drawable_update(drawable->drawable_id, 0, 0, width, height);
}

